import 'package:flutter/material.dart';
import 'package:resep_sehat_alami/views/ly_utama.dart';

class LyDashboard extends StatefulWidget {
  @override
  _LyDashboardState createState() => _LyDashboardState();
}

class _LyDashboardState extends State<LyDashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Resep Sehat Alami'),
          backgroundColor: Colors.amber,
        ),
        body: ListView(
          children: <Widget>[
        Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18.0),
      child: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  "Kategori",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 30.0,
                  ),
                ),
              ],
            ),
            new SizedBox(
              height: 10.0,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 5.0),
                    child: Container(
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Color(0xFFFD7384),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Icon(
                            Icons.local_drink,
                            color: Colors.white,
                          ),
                          new Text(
                            "Minuman",
                            style: TextStyle(color: Colors.white),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 100.0,
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                bottom: 2.5, right: 5.0),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Color(0xFF2BD093),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding:
                                    const EdgeInsets.only(right: 5.0),
                                    child: Icon(
                                      Icons.fastfood,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Text(
                                    "Makanan",
                                    style: TextStyle(color: Colors.white),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding:
                            const EdgeInsets.only(top: 2.5, right: 5.0),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Color(0xFFFC7B4D),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding:
                                    const EdgeInsets.only(right: 5.0),
                                    child: Icon(
                                      Icons.gamepad,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Text(
                                    "Obat-obatan",
                                    style: TextStyle(color: Colors.white),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 100.0,
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 2.5),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Color(0xFF53CEDB),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding:
                                    const EdgeInsets.only(right: 5.0),
                                    child: Icon(
                                      Icons.ac_unit,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Text(
                                    "Tanaman",
                                    style: TextStyle(color: Colors.white),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 2.5),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Color(0xFFF1B069),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding:
                                    const EdgeInsets.only(right: 5.0),
                                    child: Icon(
                                      Icons.dashboard,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Text(
                                    "Lain-lain",
                                    style: TextStyle(color: Colors.white),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    "Popular Resep",
                    style: TextStyle(
                        fontSize: 20.0, fontWeight: FontWeight.bold),
                  ),
                ),

              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    height: 140.0,
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 100.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5.0),
                            image: DecorationImage(
                              image: NetworkImage(
                                  "https://1.bp.blogspot.com/-NNONQnLjeGc/Xoe8djEDN1I/AAAAAAAAILk/7Pl8tIIm4wg6YB_l-ZITgoGqSPUIEy8TwCLcBGAsYHQ/s320/IMG_20200404_054114.jpg"),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Text(
                          "Resep Batuk Berdahak",
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  width: 5.0,
                ),
                Expanded(
                  child: Container(
                    height: 140.0,
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 100.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5.0),
                            image: DecorationImage(
                              image: NetworkImage(
                                  "https://1.bp.blogspot.com/-0rRTnT1Y6NY/XokK8w3uU_I/AAAAAAAAIME/LyF6649SJ_4tnb2R6RdEHC0F7AQlI8MvwCLcBGAsYHQ/s1600/IMG_20200405_052936.jpg"),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Text(
                          "Batuk Kering Untuk Dewasa",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  width: 5.0,
                ),
                Expanded(
                  child: Container(
                    height: 140.0,
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 100.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5.0),
                            image: DecorationImage(
                              image: NetworkImage(
                                  "https://i2.wp.com/muyass.com/wp-content/uploads/2019/11/muyass-wp-16.jpg?w=600&ssl=1"),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Text(
                          "Jamu Untuk Batuk dan flu",
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    "List Resep",
                    style: TextStyle(
                        fontSize: 20.0, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
              ],
            ),
          ],
        ),
      ),
    ),
            SizedBox(
              height: 20.0,
            ),
            Expanded(
              child: FlatButton(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return LyUtama();
                  }));
                },
                child: Card(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                          flex: 1,
                          child: Image.asset('assets/lake.jpg')
                      ),
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                'place.name',
                                style: TextStyle(fontSize: 16.0),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text('place.location'),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Expanded(
              child: FlatButton(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return LyUtama();
                  }));
                },
                child: Card(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                          flex: 1,
                          child: Image.asset('assets/lake.jpg')
                      ),
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                'place.name',
                                style: TextStyle(fontSize: 16.0),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text('place.location'),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Expanded(
              child: FlatButton(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return LyUtama();
                  }));
                },
                child: Card(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                          flex: 1,
                          child: Image.asset('assets/lake.jpg')
                      ),
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                'place.name',
                                style: TextStyle(fontSize: 16.0),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text('place.location'),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
    );
  }
}
