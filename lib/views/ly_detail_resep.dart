import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:resep_sehat_alami/models/m_resep.dart';

class LyDetailResep extends StatelessWidget {
  final MResep reseplist;
  LyDetailResep({@required this.reseplist});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Resep Sehat Alami-list'),
        backgroundColor: Colors.orangeAccent,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.symmetric(vertical: 10.0),
                  padding: EdgeInsets.all(16.0),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(reseplist.imageAsset)
                  )
              ),
              Card(
                margin: new EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0),
                shadowColor: Colors.black12,
                //shape: ShapeBorder.,
                //color: Colors.amberAccent,
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: new Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 15.0, right: 15.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.title),
                                //SizedBox(height: 10.0),
                                Text(reseplist.nama, style: TextStyle(fontSize: 18.0,color: Colors.grey[600]),)
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 15.0, right: 15.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.account_box),
                                //SizedBox(height: 10.0),
                                Text(reseplist.kategori, style: TextStyle(fontSize: 18.0,color: Colors.grey[600]),)
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 15.0),
                      Container(
                        padding: EdgeInsets.all(16.0),
                        child: Card(
                          child: Text(
                            reseplist.description,
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 16.0,fontFamily: 'Oxygen',),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
