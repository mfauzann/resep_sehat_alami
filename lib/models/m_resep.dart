class MResep {
  String nama;
  String kategori;
  String description;
  String imageAsset;
  List<String> imageUrls;


  MResep({
    this.nama,
    this.kategori,
    this.description,
    this.imageAsset,
    this.imageUrls,
  });
}