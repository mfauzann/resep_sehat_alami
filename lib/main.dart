import 'package:flutter/material.dart';
import 'package:resep_sehat_alami/views/ly_utama.dart';
import 'package:resep_sehat_alami/views/ly_dashboard.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //debugShowCheckedModeBanner: false,
      title: 'Resep Sehat Alami',
      theme: ThemeData.light(),
      color: Colors.amber,
      home: LyDashboard(),
    );
  }
}

